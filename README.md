# jrelax
Java开发框架，适用于企业级的通用产品项目开发，此框架的第一目的是：提高开发效率。

### 技术路线
* SpringMVC
* Spring
* Hibernate
* Memcached / Redis
* Velocity 模板
* 基于JVM的事件注册机制
* 灵活可扩展的插件机制

### 环境要求
* JDK1.8 +
* MySQL 5.6 +
* Maven 3

### 部署步骤
* 下载代码，并导入IDE（推荐使用IDEA）
* 编译代码
* 导入db目录中的数据库文件（不需要两个都导入，后续说明）
* 根据实际情况修改数据库连接，配置文件位置：src/main/resources/jdbc_mysql.properties，只需修改jdbc.master即可
* 部署到Tomcat，运行。

### 项目结构
* -- db 数据库文件存放目录
* -- jrelax-cache 缓存相关
* -- jrelax-core 核心类
* -- jrelax-fs 文件上传（独立）
* -- jrelax-kit 工具类
* -- jrelax-plugins 插件包
*    -- jrelax-plugin-fileupload 文件上传插件（比如集成aliyun oss或者七牛云等等）
*    -- jrelax-plugins-autoversion 自动对js／css资源文件加上后缀，用于避免浏览器缓存问题。 后缀规则为md5后8位
*    -- jrelax-plugins-cache 缓存插件
*    -- jrelax-plugins-db-backup 数据库备份插件
*    -- jrelax-plugins-signface 人脸识别登录插件
* -- jrelax-product 项目包
*    -- jrelax-codegen 代码生成器
*    -- jrelax-document 文档中心
*    -- jrelax-setup 安装程序（不可独立运行）
*    -- jrelax-system 框架（所有项目均依赖，可独立运行）
* -- jrelax-test 测试相关
* -- jrelax-third 集成第三方
* -- jrelax-token Token机制，用于API开放

### 简单说一下几个特性吧：
* 完善的文档
* 经过多个实际项目的检验
* 丰富的后台管理功能
* 丰富的页面布局方式
* 想不起来了。。。。

### 系统截图：
![首页](https://static.oschina.net/uploads/space/2018/0322/145226_yuYF_935028.png)
![配置页](https://static.oschina.net/uploads/space/2018/0322/145556_ZxFU_935028.png)

### 文档系统
![文档](https://static.oschina.net/uploads/space/2018/0322/150347_ptAB_935028.png)
![文档](https://static.oschina.net/uploads/space/2018/0322/150439_w3Sz_935028.png)

开源中国项目地址：https://www.oschina.net/p/jrelax

欢迎加入QQ群：490249408

如果您觉得此项目对您有价值，给作者赏一杯咖啡钱吧，哈哈哈。
<img src="https://static.oschina.net/uploads/space/2018/0322/152821_Cqkl_935028.png" width='200px' alt="支付宝"/> 
<img src="https://static.oschina.net/uploads/space/2018/0322/152832_GOYV_935028.png" width='200px' alt="微信"/>